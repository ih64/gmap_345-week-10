﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class showImage : MonoBehaviour {

    public void ShowHideImage(UnityEngine.UI.Image image)
    {
        image.gameObject.active = !image.gameObject.active;
    }
}
