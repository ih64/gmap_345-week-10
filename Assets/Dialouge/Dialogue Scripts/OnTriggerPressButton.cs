﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerPressButton : MonoBehaviour {
    public GameObject Box;
    public GameObject Dialogue;


	// Use this for initialization
	void Start () {
        Box.SetActive(false);
        Dialogue.SetActive(false);

	}
	
    void OnTriggerStay(Collider other)
    {
        Debug.Log("collider trigger");
        if (other.gameObject.tag == "Player")
        {
            Box.SetActive(true);
            Dialogue.SetActive(true);
           
        }
    }



    void OnTriggerExit()
    {
        Box.SetActive(false);
        Dialogue.SetActive(false);
        Debug.Log("exittrigger");
    }
}
