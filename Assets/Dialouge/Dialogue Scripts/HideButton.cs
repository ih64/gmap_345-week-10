﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideButton : MonoBehaviour {

        public void ShowHideButton(UnityEngine.UI.Button button)
        {
            button.gameObject.active = !button.gameObject.active;
        }
}
