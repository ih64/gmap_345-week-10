﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBoxManager : MonoBehaviour {

	public GameObject textBox;

	public Text theText;

	public TextAsset textFile;
	public string[] textLines;

	public int currentLine;
	public int endAtLine;

	public Player player;

    public bool isActive;

    public bool stopPlayerMovement;

	// Use this for initialization
	void Start () {

		//Whatever we are using as our player
		player = FindObjectOfType<Player> ();

		if (textFile != null) {
			textLines = (textFile.text.Split ('\n'));
		}

		if (endAtLine == 0) {
			endAtLine = textLines.Length - 1;
		}

        if(isActive)
        {
            EnableTextBox();
        }
        else
        {
            DisableTextBox();
        }
	}

	void Update()
    {
        if(!isActive)
        {
            return;
        }

        theText.text = textLines[currentLine];

		if (Input.GetKeyDown (KeyCode.Space)) {
			currentLine += 1;
		}

		if (currentLine > endAtLine) {
			DisableTextBox();
		} 
	}

    public void EnableTextBox()
    {
        textBox.SetActive(true);

        if(stopPlayerMovement)
        {
            //player.canMove = false;
        }
    }

    public void DisableTextBox()
    {
        textBox.SetActive(false);
        //player.canMove = true;
    }
}
