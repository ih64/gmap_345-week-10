﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour {

    //locates player
    public GameObject player;

    private Vector3 camDistance;

	
	void Start () {
        camDistance = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = player.transform.position + camDistance;
	}
}
