﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public int movementSpeed = 0;
    static Animator anim;

    private void Start()
    {
        anim = GetComponent <Animator>();
    }
    void Update()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        transform.rotation = Quaternion.LookRotation(movement);


        transform.Translate(movement * movementSpeed * Time.deltaTime, Space.World);

        if (Input.GetKey(KeyCode.D))
        {
            anim.SetBool("isWalking",true);
            anim.SetBool("isIdle", false);
            
        }

       else if (Input.GetKey(KeyCode.A))
            {
                anim.SetBool("isWalking", true);
                anim.SetBool("isIdle", false);
            }

        else if (Input.GetKey(KeyCode.W))
        {
            anim.SetBool("isWalking", true);
            anim.SetBool("isIdle", false);
        }

        else if (Input.GetKey(KeyCode.S))
        {
            anim.SetBool("isWalking", true);
            anim.SetBool("isIdle", false);
        }

        else
        {
            anim.SetBool("isIdle", true);
            anim.SetBool("isWalking", false);
        }
      
    }
}