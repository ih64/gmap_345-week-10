﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour {

    public GameObject Credits;
    public static bool CreditsUp = false;


    private void Start()
    {
        Credits.SetActive(false);
    }

   

    public void LoadGame()
    {
        SceneManager.LoadScene("Explr_MainHall - Enemy");
    }

    public void ShowCredits()
    {
        Credits.SetActive(true);
        CreditsUp = true;
    }

    public void HideCredits()
    {
        Credits.SetActive(false);
    }


}
